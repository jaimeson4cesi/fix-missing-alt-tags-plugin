<?php

declare( strict_types = 1 );
namespace FixMissingAltTags
{
	/*
	Plugin Name:  Fix Missing Alt Tags
	Plugin URI:   https://www.jaimeson-waugh.com
	Description:  Allows one to check and automatically Fix Missing Alt Tags in posts.
	Version:      1.0
	Author:       Jaimeson Waugh
	Author URI:   https://www.jaimeson-waugh.com
	License:      GPL2
	License URI:  https://www.gnu.org/licenses/gpl-2.0.html
	Text Domain:  fix-missing-alt-tags
	Domain Path:  /languages

	"Fix Missing Alt Tags" is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	"Fix Missing Alt Tags" is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with "Fix Missing Alt Tags". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
	*/

	require_once( 'fix-elements.php' );
	class FixMissingAltTags extends FixElements
	{
		//
		//  PUBLIC
		//
		/////////////////////////////////////////////////////////

			public function __construct()
			{
				parent::__construct( 'Missing Alt Tags', 'missing-alt-tags', 'is missing an alt tag' );
			}

			protected function CheckForBrokenElement( $image ) : ?array
			{
				$src = $image->getAttribute( 'src' );
				if ( $src )
				{
					if ( !$image->hasAttribute( 'alt' ) )
					{
						return [ 'src' => $src, 'dom' => $image ];
					}
				}
				return null;
			}

			protected function ChangeElement( $element ) : void
			{
				$element[ 'dom' ]->setAttribute( 'alt', '' );
			}
	}
	new FixMissingAltTags();

}
