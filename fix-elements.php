<?php

declare( strict_types = 1 );
namespace FixMissingAltTags
{
	abstract class FixElements
	{
		//
		//  PUBLIC
		//
		/////////////////////////////////////////////////////////

			public function __construct( string $name, string $slug, string $broken_message = 'is broken' )
			{
				$this->name = $name;
				$this->slug = $slug;
				$this->broken_message = $broken_message;
				add_action( 'admin_menu', [ $this, 'AddManagementPage' ] );
			}

			public function AddManagementPage() : void
			{
				add_management_page( $this->GetFullName(), $this->GetFullName(), 'administrator', $this->GetFullSlug(), [ $this, 'RenderPage' ] );
			}

			public function RenderPage() : void
			{
				?>
					<div class="wrap">
						<h1><?= $this->GetFullName(); ?></h1>
						<?php
							$this->RunController();
							$this->DrawCheckElementsButton();
							$this->DrawFixElementsButton();
						?>
					</div>
				<?php
			}



		//
		//  PROTECTED
		//
		/////////////////////////////////////////////////////////

			abstract protected function CheckForBrokenElement( $image ) : ?array;
			abstract protected function ChangeElement( $element ) : void;

			protected function TestElementIsBroken( string $url, int $level = 1 )
			{
				if ( $level >= 20 )
				{
					throw new \Exception( "$url has too many redirects." );
				}

				if ( $this->TestIsPathLocal( $url ) )
				{
					$url = 'http://www.northwestgoldcoast.com/' . $url;
				}

				$handle = curl_init( $url );
				curl_setopt( $handle, CURLOPT_NOBODY, true );
				$response = curl_exec( $handle );
				$status_code = curl_getinfo( $handle, CURLINFO_HTTP_CODE );

				switch ( $status_code )
				{
					case ( 200 ):
					{
						return false;
					}
					break;

					case ( 301 ):
					{
						return $this->TestElementIsBroken( curl_getinfo( $handle, CURLINFO_REDIRECT_URL ), $level + 1 );
					}
					break;

					case ( 404 ):
					{
						return true;
					}
					break;

					default:
					{
						echo '<p>' . $url . ' has unexpected status code: ' . $status_code . '</p>';
						return false;
					}
					break;
				}
			}



		//
		//  PRIVATE
		//
		/////////////////////////////////////////////////////////

			private function DrawCheckElementsButton() : void
			{
				$this->DrawButton( 'check-elements', 'Check for ' . $this->name . ' in all posts' );
			}

			private function DrawFixElementsButton() : void
			{
				$this->DrawButton( 'fix-elements', 'Fix all ' . $this->name . ' in all posts' );
			}

			private function DrawButton( string $button_slug, string $button_text ) : void
			{
				?>
				<form method="post" action="tools.php?page=<?= $this->GetFullSlug(); ?>" id="<?= $button_slug; ?>">
					<?php wp_nonce_field( $button_slug . '-nonce' ); ?>
					<input type="hidden" name="<?= $button_slug; ?>-action" value="true">
					<?php submit_button( $button_text ); ?>
				</form>
				<?php
			}

			private function RunController() : void
			{
				if ( isset( $_POST[ 'check-elements-action' ] ) && check_admin_referer( 'check-elements-nonce' ) )
				{
					$this->CheckElements();
				}
				else if ( isset( $_POST[ 'fix-elements-action' ] ) && check_admin_referer( 'fix-elements-nonce' ) )
				{
					$this->FixElements();
				}
			}

			private function CheckElements() : void
			{
				$this->DoWithElements
				(
					function( \WP_Post $post )
					{
						$broken_elements = $this->FindBrokenElements( $post );
						?>
							<h2><?= $this->name; ?> in <?= $post->post_title; ?></h2>
							<ul>
							<?php foreach ( $broken_elements as $broken_element ) { ?>
								<li><?= $broken_element[ 'src' ] ?> <?= $this->broken_message; ?></li>
							<?php } ?>
							</ul>
						<?php
					}
				);
			}

			private function FixElements() : void
			{
				$this->DoWithElements( [ $this, 'RemoveElements' ] );
				echo '<p>&iexcl;All ' . $this->name . ' fixed!</p>';
			}

			private function DoWithElements( callable $function ) : void
			{
				clearstatcache();
				$posts = get_posts([ 'numberposts' => -1 ]);
				foreach ( $posts as $post )
				{
					$function( $post );
				}
			}

			private function FindBrokenElements( \WP_Post $post ) : array
			{
				return $this->Elements( $post, false );
			}

			private function RemoveElements( \WP_Post $post ) : void
			{
				$this->Elements( $post, true );
			}

			private function Elements( \WP_Post $post, bool $delete ) : array
			{
				libxml_use_internal_errors( true );
				$dom = new \DOMDocument;
				$dom->loadHTML( '<?xml encoding="UTF-8">' . $post->post_content );
				$images = $dom->getElementsByTagName( 'img' );
				$broken_elements = [];

				foreach ( $images as $image )
				{
					$broken_element = $this->CheckForBrokenElement( $image );
					if ( $broken_element !== null )
					{
						array_push( $broken_elements, $broken_element );
					}
				}

				if ( $delete && !empty( $broken_elements ) )
				{
					foreach ( $broken_elements as $broken_element )
					{
						$this->ChangeElement( $broken_element );
					}

					$new_content = $dom->saveHTML();
					$post->post_content = $new_content;
					$update_status = wp_update_post( $post );
					$this->HandlePossibleUpdateErrors( $update_status );
				}

				return $broken_elements;
			}

			private function HandlePossibleUpdateErrors( $update_status ) : void
			{
				if ( is_wp_error( $update_status ) )
				{
					$errors = $update_status->get_error_messages();
					?><h2>ERRORS FOUND TRYING TO FIX BROKEN IMAGES:</h2>
					<ul><?php
					foreach ( $errors as $error )
					{
						?><li><?= $error; ?></li><?php
					}
					?></ul><?php
				}
			}

			private function TestIsPathLocal( string $path ) : bool
			{
				return parse_url( $path, PHP_URL_HOST ) === null;
			}

			private function GetFullSlug() : string
			{
				return 'fix-' . $this->slug;
			}

			private function GetFullName() : string
			{
				return 'Fix ' . $this->name;
			}

			private $name;
			private $slug;
			private $broken_message;
	}
}
